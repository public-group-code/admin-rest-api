<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;

/**
 * Class Authorize
 *
 * @package App\Http\Middleware
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class Authorize
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return JsonResponse|Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next): JsonResponse|Response|RedirectResponse
    {
        foreach ($request->user()->roles as $role){
            foreach ($role->permissions as $permission){
                if ($permission->controller_class == $request->route()->getControllerClass() &&
                    $permission->action_method == $request->route()->getActionMethod()
                    ){
                    return $next($request);
                }
            }
        }

        return response()->json(['message' => 'Unauthorized'], 401);
    }
}
