@extends('layouts.mails.master')
@section('content')
    <td class="content">
        <table>
            <tr>
                <td>
                    <p>{{ __('mails.password_reset.hello!') }}</p>
                    <p>{{ __('mails.password_reset.text_1') }}</p>
                    <p>{{ __('mails.password_reset.text_2', ['expires_at' => $expires_at]) }}</p>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <p class="code">{{ $code }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>{{ __('mails.password_reset.text_3') }}</p>
                </td>
            </tr>
        </table>
    </td>
@endsection
