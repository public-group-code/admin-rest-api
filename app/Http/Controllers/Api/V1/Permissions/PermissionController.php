<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Permissions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Permissions\StorePermissionRequest;
use App\Http\Requests\Permissions\UpdatePermissionRequest;
use App\Http\Resources\Permissions\PermissionResource;
use App\Models\Permissions\Permission;
use App\Services\Permissions\PermissionService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class PermissionController
 *
 * @package App\Http\Controllers\Permissions
 * @author <freelancer.laravel.vue@gmail.com>
 */
class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $permissions = app(PermissionService::class)->index();
        $resource = PermissionResource::collection($permissions);

        return $this->dataResponse($resource->toArray($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePermissionRequest  $request
     * @return JsonResponse
     */
    public function store(StorePermissionRequest $request): JsonResponse
    {
        $permission = app(PermissionService::class)->store(
            $request->name,
            $request->controller_class,
            $request->action_method
        );
        app(PermissionService::class)->attachRolesToPermission(
            $permission,
            $request->role_ids
        );

        return $this->successResponseWithoutContent();
    }

    /**
     * Display the specified resource.
     *
     * @param Permission $permission
     * @return JsonResponse
     */
    public function show(Permission $permission): JsonResponse
    {
        $resource = new PermissionResource($permission);

        return $this->dataResponse($resource->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePermissionRequest $request
     * @param Permission $permission
     * @return JsonResponse
     */
    public function update(UpdatePermissionRequest $request, Permission $permission): JsonResponse
    {
        app(PermissionService::class)->update(
            $permission,
            $request->name,
            $request->controller_class,
            $request->action_method
        );
        app(PermissionService::class)->syncRolesToPermission(
            $permission,
            $request->role_ids
        );

        return $this->successResponseWithoutContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Permission $permission
     * @return JsonResponse
     */
    public function destroy(Permission $permission): JsonResponse
    {
        $permission->roles()->detach();
        $permission->delete();

        return $this->successResponseWithoutContent();
    }
}
