<?php
declare(strict_types=1);

namespace App\Validators\Users;

use App\Exceptions\Users\DeviceNameMaxLengthInvalidException;

/**
 * Class CreateUserValidator
 *
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class CreateUserValidator
{
    /**
     * @var int Max password length.
     */
    const MAX_LENGTH_PASSWORD = 32;

    /**
     * @var int Min password length.
     */
    const MIN_LENGTH_PASSWORD = 8;

    /**
     * @var int Max device name length.
     */
    const MAX_LENGTH_DEVICE_NAME = 255;

    /**
     * Validate device name.
     *
     * @param string $deviceName
     */
    public static function validateDeviceName(string $deviceName): void
    {
        if (strlen($deviceName) > self::MAX_LENGTH_DEVICE_NAME) {
            throw new DeviceNameMaxLengthInvalidException(self::MAX_LENGTH_DEVICE_NAME);
        }
    }
}
