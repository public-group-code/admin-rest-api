<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\StoreUserRequest;
use App\Http\Requests\Users\UpdateUserRequest;
use App\Http\Resources\Users\UserResource;
use App\Models\User;
use App\Services\Users\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\Api\V1\User
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $users = app(UserService::class)->index();
        $resource = UserResource::collection($users);

        return $this->dataResponse($resource->toArray($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreUserRequest  $request
     * @return JsonResponse
     */
    public function store(StoreUserRequest $request): JsonResponse
    {
        $user = app(UserService::class)->store(
            $request->name,
            $request->email,
            $request->password
        );
        app(UserService::class)->attachRolesToUser(
            $user,
            $request->role_ids
        );

        return $this->successResponseWithoutContent();
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function show(User $user): JsonResponse
    {
        $resource = new UserResource($user);

        return $this->dataResponse($resource->toArray());
    }

    /**
     * getMe
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getMe(Request $request): JsonResponse
    {
        return $this->dataResponse((new UserResource($request->user()))->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @return JsonResponse
     */
    public function update(UpdateUserRequest $request, User $user): JsonResponse
    {
        app(UserService::class)->update(
            $user,
            $request->name,
            $request->email,
            $request->password
        );
        app(UserService::class)->syncRolesToUser(
            $user,
            $request->role_ids
        );

        return $this->successResponseWithoutContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function destroy(User $user): JsonResponse
    {
        app(UserService::class)->destroy($user);

        return $this->successResponseWithoutContent();
    }
}
