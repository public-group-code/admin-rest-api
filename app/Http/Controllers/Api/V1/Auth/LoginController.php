<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\Auth\SuccessAuthResource;
use App\Services\Auth\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Auth\LogoutRequest;

/**
 * Class LoginController
 *
 * @package App\Http\Controllers\Api\V1\Auth
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout', 'logoutFromAll']);
    }

    public function login(Request $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');

        if (!Auth::attempt($credentials)) {
            return $this->errorResponse('You cannot sign with those credentials');
        }

        $token = app(AuthService::class)->authenticate(
            Auth::user(), 
            $request->input('device_name'), 
            $request->input('remember_me')
        );


        return $this->dataResponse((new SuccessAuthResource($token))->toArray());
    }
    
    /**
     * Logout user.
     *
     * @param LogoutRequest $request
     * @return JsonResponse
     */
    public function logout(LogoutRequest $request): JsonResponse
    {
        Auth::user()->tokens->each(function ($token, $key) use ($request) {
            if ($token->id === $request->input('token_id')) {
                $token->delete();
            }
            
            return;
        });

        return $this->successResponseWithoutContent();
    }
    
    /**
     * Logout user from all devices.
     *
     * @return JsonResponse
     */
    public function logoutFromAll(): JsonResponse
    {
        Auth::user()->tokens()->delete();

        return $this->successResponseWithoutContent();
    }
}
