<?php
declare(strict_types=1);

namespace App\Http\Requests\Roles;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRoleRequest
 *
 * @package App\Http\Requests\Roles
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class StoreRoleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => [
                'bail',
                'required',
                'string',
                'max:255'
            ],
            'slug' => [
                'bail',
                'required',
                'string',
                'max:255',
                'unique:roles'
            ],
        ];
    }
}
