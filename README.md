<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Start with Docker (Sail) Laravel
    1) git clone git@gitlab.com:public-group-code/admin-rest-api.git your_app_name
    2) cd your_app_name
    3) cp .env.example .env
    4)You need to change the following in your file .env:
        DB_CONNECTION=mysql
        DB_HOST={your_app_name}_mysql_1
        DB_PORT=3306
        DB_DATABASE={your_app_name}_db
        DB_USERNAME=sail
        DB_PASSWORD=password
    5) docker run --rm \
        -u "$(id -u):$(id -g)" \
        -v "$(pwd):/var/www/html" \
        -w /var/www/html \
        laravelsail/php82-composer:latest \
        composer install --ignore-platform-reqs
    6) ./vendor/bin/sail up -d
    7) ./vendor/bin/sail php artisan storage:link
    8) ./vendor/bin/sail php artisan key:generate
    9) ./vendor/bin/sail php artisan migrate:refresh --seed
    10) ./vendor/bin/sail php artisan passport:install


### If the database needs to be refresh, follow this command.
    
    1) ./vendor/bin/sail php artisan migrate:refresh --seed && ./vendor/bin/sail php artisan passport:install
