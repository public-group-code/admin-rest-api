<?php
declare(strict_types=1);

namespace App\Http\Resources\Roles;

use App\Http\Resources\Permissions\PermissionResource;
use App\Http\Resources\Users\UserRoleResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class RoleResource
 *
 * @package App\Http\Resources\Roles
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request = null): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'permissions' => PermissionResource::collection($this->permissions),
            'users' => UserRoleResource::collection($this->users),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
