<?php

namespace App\Providers;

use Laravel\Passport\Console\InstallCommand;
use Laravel\Passport\Console\ClientCommand;
use Laravel\Passport\Console\HashCommand;
use Laravel\Passport\Console\KeysCommand;
use Laravel\Passport\Console\PurgeCommand;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->commands([
            InstallCommand::class,
            ClientCommand::class,
            HashCommand::class,
            KeysCommand::class,
            PurgeCommand::class,
        ]);
    }
}
