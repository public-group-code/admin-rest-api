<?php
declare(strict_types=1);

namespace App\Traits;

use App\Http\Controllers\Api\V1\Permissions\PermissionController;
use App\Http\Controllers\Api\V1\Roles\RoleController;
use App\Http\Controllers\Api\V1\Users\UserController;
use App\Models\Permissions\Permission;
use App\Models\Roles\Role;
use App\Services\Auth\AuthService;
use Illuminate\Support\Facades\Hash;
use \App\Models\User;

/**
 * Trait AuthTest
 *
 * @package App\Traits
 * @author <freelancer.laravel.vue@gmail.com>
 */
trait AuthTest 
{
    /**
     * Get Registered User
     * 
     * @return User
     */
    protected function getRegisteredUser(): User
    {
        return User::create([
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'password' => Hash::make('password')
        ]);
    }
    
    /**
     * Get Token Data
     * 
     * @param User $user
     * @return object
     */
    protected function getTokenData(User $user): object 
    {
        $token = app(AuthService::class)->authenticate(
            $user, 
            'Iphone 14 PRO MAX', 
            '10'
        );
        
        return $token;
    }
    
    /**
     * Get Permission Permissions
     * 
     * @return array
     */
    protected function getPermissionPermissions(): array 
    {
        return [
            [
                'name' => 'Permissions Index',
                'controller_class' => PermissionController::class,
                'action_method' => 'index',
            ],
            [
                'name' => 'Permissions Store',
                'controller_class' => PermissionController::class,
                'action_method' => 'store',
            ],
            [
                'name' => 'Permission Show',
                'controller_class' => PermissionController::class,
                'action_method' => 'show',
            ],
            [
                'name' => 'Permission Update',
                'controller_class' => PermissionController::class,
                'action_method' => 'update',
            ],
            [
                'name' => 'Permission Delete',
                'controller_class' => PermissionController::class,
                'action_method' => 'destroy',
            ]
        ];
    }
    
    /**
     * Get Permission Roles
     * 
     * @return array
     */
    protected function getPermissionRoles(): array 
    {
        return [
            [
                'name' => 'Role Index',
                'controller_class' => RoleController::class,
                'action_method' => 'index',
            ],
            [
                'name' => 'Role Store',
                'controller_class' => RoleController::class,
                'action_method' => 'store',
            ],
            [
                'name' => 'Role Show',
                'controller_class' => RoleController::class,
                'action_method' => 'show',
            ],
            [
                'name' => 'Role Update',
                'controller_class' => RoleController::class,
                'action_method' => 'update',
            ],
            [
                'name' => 'Role Delete',
                'controller_class' => RoleController::class,
                'action_method' => 'destroy',
            ]
        ];
    }
    
    /**
     * Get Permission Users
     * 
     * @return array
     */
    protected function getPermissionUsers(): array 
    {
        return [
            [
                'name' => 'User Index',
                'controller_class' => UserController::class,
                'action_method' => 'index',
            ],
            [
                'name' => 'User Store',
                'controller_class' => UserController::class,
                'action_method' => 'store',
            ],
            [
                'name' => 'User Show',
                'controller_class' => UserController::class,
                'action_method' => 'show',
            ],
            [
                'name' => 'User Update',
                'controller_class' => UserController::class,
                'action_method' => 'update',
            ],
            [
                'name' => 'User Delete',
                'controller_class' => UserController::class,
                'action_method' => 'destroy',
            ],
            [
                'name' => 'User Me',
                'controller_class' => UserController::class,
                'action_method' => 'getMe',
            ]
        ];
    }

    /**
     * Get Permissions All
     * 
     * @return array
     */
    protected function getPermissionsAll(): array
    {
        return [
            'permission_permissions' => $this->getPermissionPermissions(),
            'permission_roles' => $this->getPermissionRoles(),
            'permission_users' => $this->getPermissionUsers(),
        ];
    }
    
    /**
     * Get Created Permission Ids
     * 
     * @param array $permissions
     * @return array
     */
    protected function getCreatedPermissionIds(array $permissions): array
    {
        $ids = [];
        
        foreach ($permissions as $permission_types){
            foreach ($permission_types as $permission){
                $permission_created = Permission::create([
                    'name' => $permission['name'],
                    'controller_class' => $permission['controller_class'],
                    'action_method' => $permission['action_method'],
                ]);
                
                array_push($ids, $permission_created->id);
            }
        }
        
        return $ids;
    }
    
    /**
     * Get Created Role
     * 
     * @param User $user
     * @return type
     */
    protected function getCreatedRole(User $user): Role
    {
        return Role::create([
            'name' => $user->name,
            'slug' => strtolower($user->name),
        ]);
    }
    
    /**
     * Give Role Permissions
     * 
     * @param Role $role
     * @param array $permissionIds
     * @return void
     */
    protected function giveRolePermissions(Role $role, array $permissionIds): void 
    {
        $role->permissions()->attach($permissionIds);
    }
    
    /**
     * Give User Role With Permissions
     * 
     * @param User $user
     * @return void
     */
    protected function giveUserRoleWithPermissions(User $user): void 
    {
        $permissionIds = $this->getCreatedPermissionIds($this->getPermissionsAll());
        $role = $this->getCreatedRole($user);
        $this->giveRolePermissions($role, $permissionIds);
        $user->roles()->attach($role);
    }
}
