<?php
declare(strict_types=1);

namespace App\Exceptions\Users;

use Illuminate\Http\Response;
use InvalidArgumentException;
use Throwable;

/**
 * Class DeviceNameMaxLengthInvalidException
 *
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class DeviceNameMaxLengthInvalidException extends InvalidArgumentException implements Throwable
{
    /**
     * DeviceNameMaxLengthInvalidException constructor.
     *
     * @param int $length
     */
    public function __construct(int $length)
    {
        $message = __('exceptions.users.deviceNameMaxLength', ['length' => $length]);

        parent::__construct($message, Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
