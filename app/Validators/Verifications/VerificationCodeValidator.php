<?php
declare(strict_types=1);

namespace App\Validators\Verifications;

/**
 * Class VerificationCodeValidator
 *
 * @package App\Validators\Verifications
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class VerificationCodeValidator 
{
    /**
     * @var int Max length code
     */
    const MAX_LENGTH_CODE = 99999;
    
    /**
     * @var int Min length code
     */
    const MIN_LENGTH_CODE = 10000;
}
