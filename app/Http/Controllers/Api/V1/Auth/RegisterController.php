<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Auth;

use Illuminate\Auth\Events\Registered;
use App\DTO\Factories\Auth\UserRegisterDTOFactory;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UserRegisterRequest;
use App\Services\Auth\AuthService;
use Illuminate\Http\JsonResponse;

/**
 * Class RegisterController
 *
 * @package App\Http\Controllers\Api\V1\Auth
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * User Register
     *
     * @param UserRegisterRequest $request
     * @return JsonResponse
     */
    public function register(UserRegisterRequest $request): JsonResponse
    {
        $DTO = app(UserRegisterDTOFactory::class)->createFromRequest($request);
        $user = app(AuthService::class)->register($DTO);
        
        event(new Registered($user));

        return $this->successResponseWithoutContent();
    }
}
