<?php
declare(strict_types=1);

namespace App\Repositories\Permissions;

use Illuminate\Database\Eloquent\Collection;
use App\Models\Permissions\Permission;

/**
 * Class PermissionRepository
 *
 * @package App\Repositories\Permissions
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PermissionRepository 
{
    /**
     * Index
     * 
     * @return Collection
     */
    public function index(): Collection
    {
        return Permission::with('roles')
            ->orderBy('created_at', 'desc')
            ->get();
    }
    
    /**
     * Return created Permission
     *
     * @param string $name
     * @param string $controller_class
     * @param string $action_method
     * @return Permission
     */
    public function store(string $name, string $controller_class, string $action_method): Permission
    {
        return Permission::create([
            'name' => $name,
            'controller_class' => $controller_class,
            'action_method' => $action_method,
        ]);
    }

    /**
     * Return bool
     *
     * @param Permission $permission
     * @param string $name
     * @param string $controller_class
     * @param string $action_method
     * @return bool
     */
    public function update(Permission $permission, string $name, string $controller_class, string $action_method): bool
    {
        return $permission->update([
            'name' => $name,
            'controller_class' => $controller_class,
            'action_method' => $action_method,
        ]);
    }
}
