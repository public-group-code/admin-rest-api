<?php
declare(strict_types=1);

namespace App\Http\Requests\Roles;

use App\Models\Roles\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateRoleRequest
 *
 * @package App\Http\Requests\Roles
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class UpdateRoleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => [
                'bail',
                'required',
                'string',
                'max:255',
            ],
            'slug' => [
                'bail',
                'required',
                'string',
                'max:255',
                Rule::unique('roles')->ignore($this->role),
            ],
        ];
    }
}
