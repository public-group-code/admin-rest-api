<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Http\Controllers\Api\V1\Permissions\PermissionController;
use App\Http\Controllers\Api\V1\Roles\RoleController;
use App\Http\Controllers\Api\V1\Users\UserController;
use App\Models\Permissions\Permission;
use App\Models\Roles\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

/**
 * Class PermissionsSeeder
 *
 * @package Database\Seeders
 * @author <freelancer.laravel.vue@gmail.com>
 */
class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        /**
         *
         * Create Permissions For Controllers
         *
         */

        //Permissions Permissions
        $permissionIndex = Permission::create([
            'name' => 'Permissions Index',
            'controller_class' => PermissionController::class,
            'action_method' => 'index',
        ]);
        $permissionStore = Permission::create([
            'name' => 'Permissions Store',
            'controller_class' => PermissionController::class,
            'action_method' => 'store',
        ]);
        $permissionShow = Permission::create([
            'name' => 'Permission Show',
            'controller_class' => PermissionController::class,
            'action_method' => 'show',
        ]);
        $permissionUpdate = Permission::create([
            'name' => 'Permission Update',
            'controller_class' => PermissionController::class,
            'action_method' => 'update',
        ]);
        $permissionDelete = Permission::create([
            'name' => 'Permission Delete',
            'controller_class' => PermissionController::class,
            'action_method' => 'destroy',
        ]);

        //Permissions Roles
        $permissionRoleIndex = Permission::create([
            'name' => 'Role Index',
            'controller_class' => RoleController::class,
            'action_method' => 'index',
        ]);
        $permissionRoleStore = Permission::create([
            'name' => 'Role Store',
            'controller_class' => RoleController::class,
            'action_method' => 'store',
        ]);
        $permissionRoleShow = Permission::create([
            'name' => 'Role Show',
            'controller_class' => RoleController::class,
            'action_method' => 'show',
        ]);
        $permissionRoleUpdate = Permission::create([
            'name' => 'Role Update',
            'controller_class' => RoleController::class,
            'action_method' => 'update',
        ]);
        $permissionRoleDelete = Permission::create([
            'name' => 'Role Delete',
            'controller_class' => RoleController::class,
            'action_method' => 'destroy',
        ]);

        //Permissions Users
        $permissionUserIndex = Permission::create([
            'name' => 'User Index',
            'controller_class' => UserController::class,
            'action_method' => 'index',
        ]);
        $permissionUserStore = Permission::create([
            'name' => 'User Store',
            'controller_class' => UserController::class,
            'action_method' => 'store',
        ]);
        $permissionUserShow = Permission::create([
            'name' => 'User Show',
            'controller_class' => UserController::class,
            'action_method' => 'show',
        ]);
        $permissionUserGetMe = Permission::create([
            'name' => 'User GetMe',
            'controller_class' => UserController::class,
            'action_method' => 'getMe',
        ]);
        $permissionUserUpdate = Permission::create([
            'name' => 'User Update',
            'controller_class' => UserController::class,
            'action_method' => 'update',
        ]);
        $permissionUserDelete = Permission::create([
            'name' => 'User Delete',
            'controller_class' => UserController::class,
            'action_method' => 'destroy',
        ]);

        /**
         *
         * Create Roles
         *
         */
        $roleAdmin = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);
        $permissionIds = [
            //Permissions
            $permissionIndex->id,
            $permissionStore->id,
            $permissionShow->id,
            $permissionUpdate->id,
            $permissionDelete->id,
            //Roles
            $permissionRoleIndex->id,
            $permissionRoleStore->id,
            $permissionRoleShow->id,
            $permissionRoleUpdate->id,
            $permissionRoleDelete->id,
            //Users
            $permissionUserIndex->id,
            $permissionUserStore->id,
            $permissionUserShow->id,
            $permissionUserGetMe->id,
            $permissionUserUpdate->id,
            $permissionUserDelete->id,
        ];
        //Give role permissions
        $roleAdmin->permissions()->attach($permissionIds);

        /**
         *
         * Create Users
         *
         */
        $userOne = User::create([
            'name' => 'User One',
            'email' => 'userone@mail.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
        ]);
        //Give user roles
        $userOne->roles()->attach($roleAdmin);
    }
}
