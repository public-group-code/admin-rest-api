<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;

/**
 * Class StorePermissionRequest
 *
 * @package App\Http\Requests\Permissions
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class IsExpiredToken
{
    /**
     * Handle an incoming request.
     *
     * @param Request  $request
     * @param Closure $next
     * @return JsonResponse|Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next): JsonResponse|Response|RedirectResponse
    {
        $user = $request->user();
        $data_now = Carbon::now();
        $expires_at = $user->token()->expires_at;
            
        if ($data_now > $expires_at) {
            $user->token()->delete();
            
            return new JsonResponse([
                'message' => 'Token has expired'
            ], 403);
            
        }
        
        return $next($request);
    }
}
