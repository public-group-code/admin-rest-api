<?php
declare(strict_types=1);

namespace App\Http\Requests\Auth;

use App\Validators\Users\CreateUserValidator;
use App\Validators\Verifications\VerificationCodeValidator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ForgotPasswordRequest
 *
 * @package App\Http\Requests\Auth
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class ResetPasswordRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email'
            ],
            'code' => [
                'required', 
                'integer', 
                'min:' . VerificationCodeValidator::MIN_LENGTH_CODE, 
                'max:' . VerificationCodeValidator::MAX_LENGTH_CODE
            ],
            'password' => [
                'required', 
                'confirmed', 
                'min:' . CreateUserValidator::MIN_LENGTH_PASSWORD, 
                'max:' . CreateUserValidator::MAX_LENGTH_PASSWORD
            ],
        ];
    }
}

