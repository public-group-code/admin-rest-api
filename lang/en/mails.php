<?php
declare(strict_types=1);

return [
    'email_verification' => [
        'hello!' => 'Hello!',
        'text_1' => 'You have registered in this application. verification code is below.',
        'text_2' => 'This email verification code will expire in :expires_at minutes.',
        'text_3' => 'If you have not registered this application, then you will ignore this message.',
    ],
    'password_reset' => [
        'hello!' => 'Hello!',
        'text_1' => 'You are receiving this email because we received a password reset request for your account.',
        'text_2' => 'This password reset code will expire in :expires_at minutes.',
        'text_button' => 'Reset Password',
        'text_3' => 'If you did not request a password reset, no further action is required.',
    ],
];
