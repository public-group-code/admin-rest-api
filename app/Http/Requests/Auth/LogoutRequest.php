<?php
declare(strict_types=1);

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class LogoutRequest
 *
 * @package App\Http\Requests\Auth
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LogoutRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'token_id' => 'bail|required|string',
        ];
    }
}
