<?php
declare(strict_types=1);

namespace App\Http\Resources\Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SuccessAuthResource
 *
 * @package App\Http\Resources\Auth
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class SuccessAuthResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request = null): array
    {
        return [
            'id' => $this->token->id,
            'token_type' => 'Bearer',
            'token' => $this->accessToken,
            'expires_at' => Carbon::parse($this->token->expires_at)->toDateTimeString(),
        ];
    }
}
