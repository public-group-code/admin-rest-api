<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Traits\AuthTest;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

/**
 * class RolesTest
 *
 * @package Tests\Feature
 * @author <freelancer.laravel.vue@gmail.com>
 */
class RolesTest extends TestCase
{
    use RefreshDatabase, AuthTest;
    
    public function test_index(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->get('api/roles/', 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(200);
    }
    
    public function test_fail_unauthorized_index(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $response = $this->get('api/roles/', 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $permissionIds = $this->getCreatedPermissionIds($this->getPermissionsAll());
        $response = $this->post('api/roles/', 
            [
                'name' => 'New Role',
                'slug' => 'newrole',
                'permission_ids' => $permissionIds,
                'user_ids' => [$user->id]
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertNoContent($status = 204);
    }
    
    public function test_fail_unauthorized_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $permissionIds = $this->getCreatedPermissionIds($this->getPermissionsAll());
        $response = $this->post('api/roles/', 
            [
                'name' => 'New Role',
                'slug' => 'newrole',
                'permission_ids' => $permissionIds,
                'user_ids' => [$user->id]
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_fail_name_required_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $permissionIds = $this->getCreatedPermissionIds($this->getPermissionsAll());
        $response = $this->post('api/roles/', 
            [
                'name' => '',
                'slug' => 'newrole',
                'permission_ids' => $permissionIds,
                'user_ids' => [$user->id]
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['name' => 'The name field is required.']);
    }
    
    public function test_fail_slug_required_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $permissionIds = $this->getCreatedPermissionIds($this->getPermissionsAll());
        $response = $this->post('api/roles/', 
            [
                'name' => 'New Role',
                'slug' => '',
                'permission_ids' => $permissionIds,
                'user_ids' => [$user->id]
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['slug' => 'The slug field is required.']);
    }
    
    public function test_show(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        $response = $this->get('api/roles/show/' . $role->id, 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(200);
    }
    
    public function test_fail_unauthorized_show(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $userTwo = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($userTwo);
        $role = $userTwo->roles()->first();
        $response = $this->get('api/roles/show/' . $role->id, 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $permissionIds = $this->getCreatedPermissionIds($this->getPermissionsAll());
        $role = $user->roles()->first();
        $response = $this->put('api/roles/update/' . $role->id, 
            [
                'name' => 'New Role',
                'slug' => 'newrole',
                'permission_ids' => $permissionIds,
                'user_ids' => [$user->id]
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertNoContent($status = 204);
    }
    
    public function test_fail_unauthorized_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $userTwo = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($userTwo);
        $permissionIds = $this->getCreatedPermissionIds($this->getPermissionsAll());
        $role = $userTwo->roles()->first();
        $response = $this->put('api/roles/update/' . $role->id, 
            [
                'name' => 'New Role',
                'slug' => 'newrole',
                'permission_ids' => $permissionIds,
                'user_ids' => [$user->id]
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_fail_name_required_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $permissionIds = $this->getCreatedPermissionIds($this->getPermissionsAll());
        $role = $user->roles()->first();
        $response = $this->put('api/roles/update/' . $role->id, 
            [
                'name' => '',
                'slug' => 'newrole',
                'permission_ids' => $permissionIds,
                'user_ids' => [$user->id]
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['name' => 'The name field is required.']);
    }
    
    public function test_fail_slug_required_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $permissionIds = $this->getCreatedPermissionIds($this->getPermissionsAll());
        $role = $user->roles()->first();
        $response = $this->put('api/roles/update/' . $role->id, 
            [
                'name' => 'New Role',
                'slug' => '',
                'permission_ids' => $permissionIds,
                'user_ids' => [$user->id]
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['slug' => 'The slug field is required.']);
    }
    
    public function test_delete(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        $response = $this->delete('api/roles/delete/' . $role->id, 
            [],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertNoContent($status = 204);
    }
    
    public function test_fail_unauthorized_delete(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $userTwo = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($userTwo);
        $role = $userTwo->roles()->first();
        $response = $this->delete('api/roles/delete/' . $role->id, 
            [],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
}
