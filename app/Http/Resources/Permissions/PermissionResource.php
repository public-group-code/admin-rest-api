<?php
declare(strict_types=1);

namespace App\Http\Resources\Permissions;

use App\Http\Resources\Roles\RolePermissionResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PermissionResource
 *
 * @package App\Http\Resources\Permissions
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PermissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request = null): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'controller_class' => $this->controller_class,
            'action_method' => $this->action_method,
            'roles' => RolePermissionResource::collection($this->roles),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
