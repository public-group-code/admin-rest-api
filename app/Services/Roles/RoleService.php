<?php
declare(strict_types=1);

namespace App\Services\Roles;

use App\Repositories\Roles\RoleRepository;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Roles\Role;

/**
 * Class RoleService
 *
 * @package App\Services\Users
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class RoleService
{
    /**
     * Index
     * 
     * @return Collection
     */
    public function index(): Collection
    {
        return app(RoleRepository::class)->index();
    }
    
    /**
     * Return created Role
     *
     * @param string $name
     * @param string $slug
     * @return Role
     */
    public function store(string $name, string $slug): Role
    {
        return app(RoleRepository::class)->store(
            $name,
            $slug
        );
    }

    /**
     * Return bool
     *
     * @param Role $role
     * @param string $name
     * @param string $slug
     * @return bool
     */
    public function update(Role $role, string $name, string $slug): bool
    {
        return app(RoleRepository::class)->update(
            $role,
            $name,
            $slug
        );
    }

    /**
     * givePermissionToRole
     *
     * @param Role $role
     * @param array|null $permission_ids
     * @return void
     */
    public function attachPermissionsToRole(Role $role, ?array $permission_ids): void
    {
        $role->permissions()->attach($permission_ids);
    }

    /**
     * syncPermissionToRole
     *
     * @param Role $role
     * @param array|null $permission_ids
     * @return void
     */
    public function syncPermissionsToRole(Role $role, ?array $permission_ids): void
    {
        $role->permissions()->sync($permission_ids);
    }

    /**
     * attachUsersToRole
     *
     * @param Role $role
     * @param array|null $user_ids
     * @return void
     */
    public function attachUsersToRole(Role $role, ?array $user_ids): void
    {
        $role->users()->attach($user_ids);
    }

    /**
     * syncUsersToRole
     *
     * @param Role $role
     * @param array|null $user_ids
     * @return void
     */
    public function syncUsersToRole(Role $role, ?array $user_ids): void
    {
        $role->users()->sync($user_ids);
    }
    
    /**
     * Destroy
     * 
     * @param Role $role
     * @return void
     */
    public function destroy(Role $role): void 
    {
        $role->permissions()->detach();
        $role->delete();
    }
}
