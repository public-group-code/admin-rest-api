<?php
declare(strict_types=1);

namespace Tests\Feature;

use App\Traits\AuthTest;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

class PermissionsTest extends TestCase
{
    use RefreshDatabase, AuthTest;
    
    public function test_index(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->get('api/permissions/', 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(200);
    }
    
    public function test_fail_unauthorized_index(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $response = $this->get('api/permissions/', 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        
        $response = $this->post('api/permissions/', 
            [
                'name' => 'User Addition',
                'controller_class' => 'App\Http\Controllers\Api\V1\Users\UserController',
                'action_method' =>'addition',
                'role_ids' => [$role->id],
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertNoContent($status = 204);
    }
    
    public function test_fail_unauthorized_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $userTwo = $this->getRegisteredUser();
        $this->giveUserRoleWithPermissions($userTwo);
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $role = $userTwo->roles()->first();
        
        $response = $this->post('api/permissions/', 
            [
                'name' => 'User Addition',
                'controller_class' => 'App\Http\Controllers\Api\V1\Users\UserController',
                'action_method' =>'addition',
                'role_ids' => [$role->id],
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_fail_name_required_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        
        $response = $this->post('api/permissions/', 
            [
                'name' => '',
                'controller_class' => 'App\Http\Controllers\Api\V1\Users\UserController',
                'action_method' =>'addition',
                'role_ids' => [$role->id],
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['name' => 'The name field is required.']);
    }
    
    public function test_fail_controller_class_required_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        
        $response = $this->post('api/permissions/', 
            [
                'name' => 'User Addition',
                'controller_class' => '',
                'action_method' =>'addition',
                'role_ids' => [$role->id],
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['controller_class' => 'The controller class field is required.']);
    }
    
    public function test_fail_action_method_required_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        
        $response = $this->post('api/permissions/', 
            [
                'name' => 'User Addition',
                'controller_class' => 'App\Http\Controllers\Api\V1\Users\UserController',
                'action_method' =>'',
                'role_ids' => [$role->id],
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['action_method' => 'The action method field is required.']);
    }
    
    public function test_show(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        $permission = $role->permissions()->first();
        
        $response = $this->get('api/permissions/show/' . $permission->id, 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(200);
    }
    
    public function test_fail_unauthorized_show(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $userTwo = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($userTwo);
        $role = $userTwo->roles()->first();
        $permission = $role->permissions()->first();
        
        $response = $this->get('api/permissions/show/' . $permission->id, 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        $permission = $role->permissions()->first();
        
        $response = $this->put('api/permissions/update/' . $permission->id, 
            [
                'name' => 'User Addition',
                'controller_class' => 'App\Http\Controllers\Api\V1\Users\UserController',
                'action_method' =>'addition',
                'role_ids' => [$role->id],
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertNoContent($status = 204);
    }
    
    public function test_fail_unauthorized_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $userTwo = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($userTwo);
        $role = $userTwo->roles()->first();
        $permission = $role->permissions()->first();
        
        $response = $this->put('api/permissions/update/' . $permission->id, 
            [
                'name' => 'User Addition',
                'controller_class' => 'App\Http\Controllers\Api\V1\Users\UserController',
                'action_method' =>'addition',
                'role_ids' => [$role->id],
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_fail_name_required_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        $permission = $role->permissions()->first();
        
        $response = $this->put('api/permissions/update/' . $permission->id, 
            [
                'name' => '',
                'controller_class' => 'App\Http\Controllers\Api\V1\Users\UserController',
                'action_method' =>'addition',
                'role_ids' => [$role->id],
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['name' => 'The name field is required.']);
    }
    
    public function test_fail_controller_class_required_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        $permission = $role->permissions()->first();
        
        $response = $this->put('api/permissions/update/' . $permission->id, 
            [
                'name' => 'User Addition',
                'controller_class' => '',
                'action_method' =>'addition',
                'role_ids' => [$role->id],
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['controller_class' => 'The controller class field is required.']);
    }
    
    public function test_fail_action_method_required_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        $permission = $role->permissions()->first();
        
        $response = $this->put('api/permissions/update/' . $permission->id, 
            [
                'name' => 'User Addition',
                'controller_class' => 'App\Http\Controllers\Api\V1\Users\UserController',
                'action_method' =>'',
                'role_ids' => [$role->id],
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['action_method' => 'The action method field is required.']);
    }
    
    public function test_delete(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $role = $user->roles()->first();
        $permission = $role->permissions()->first();
        
        $response = $this->delete('api/permissions/delete/' . $permission->id, 
            [],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertNoContent($status = 204);
    }
    
    public function test_fail_unauthorized_delete(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $userTwo = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($userTwo);
        $role = $userTwo->roles()->first();
        $permission = $role->permissions()->first();
        
        $response = $this->delete('api/permissions/delete/' . $permission->id, 
            [],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
}
