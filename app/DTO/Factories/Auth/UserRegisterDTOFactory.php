<?php
declare(strict_types=1);

namespace App\DTO\Factories\Auth;

use App\DTO\Auth\UserRegisterDTO;
use Illuminate\Http\Request;

/**
 * Class UserRegisterDTOFactory
 *
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class UserRegisterDTOFactory
{
    /**
     * Return UserRegisterDTO
     *
     * @param Request $request
     * @return UserRegisterDTO
     */
    public static function createFromRequest(Request $request): UserRegisterDTO
    {
        return self::createFromArray($request->validated());
    }

    /**
     * Return UserRegisterDTO
     *
     * @param array $data
     * @return UserRegisterDTO
     */
    public static function createFromArray(array $data): UserRegisterDTO
    {
        return new UserRegisterDTO(
            (string) $data['name'],
            (string) $data['email'],
            (string) $data['password'],
        );
    }
}
