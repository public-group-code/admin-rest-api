<?php
declare(strict_types=1);

namespace App\Http\Requests\Users;

use App\Validators\Users\CreateUserValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateUserRequest
 *
 * @package App\Http\Requests\Users
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class UpdateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => [
                'bail',
                'required',
                'string',
                'max:255'
            ],
            'email' => [
                'bail',
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($this->user),
            ],
            'password' => [
                'bail',
                'required',
                'string',
                'min:' . CreateUserValidator::MIN_LENGTH_PASSWORD,
                'max:' . CreateUserValidator::MAX_LENGTH_PASSWORD,
                'confirmed'
            ],
        ];
    }
}
