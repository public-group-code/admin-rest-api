<?php
declare(strict_types=1);

namespace App\Http\Requests\Auth;

use App\Validators\Verifications\VerificationCodeValidator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class VerificationCodeRequest
 *
 * @package App\Http\Requests\Auth
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class VerificationCodeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'code' => [
                'required', 
                'integer', 
                'min:' . VerificationCodeValidator::MIN_LENGTH_CODE, 
                'max:' . VerificationCodeValidator::MAX_LENGTH_CODE
            ]
        ];
    }
}
