<?php
declare(strict_types=1);

namespace App\Services\Verifications;

use App\Validators\Verifications\VerificationCodeValidator;
use App\Models\User;
use Illuminate\Support\Carbon;
use App\Models\Verifications\VerificationCode;

/**
 * Class VerificationCodeService
 *
 * @package App\Services\Verifications
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class VerificationCodeService 
{
    /**
     * Create verification code
     * 
     * @param User $user
     * @param string $type
     * @param int $expires_at
     * @return VerificationCode
     */
    public function create(User $user, string $type, int $expires_at): VerificationCode 
    {
        return VerificationCode::create([
            'user_id' => $user->id,
            'code' => rand(VerificationCodeValidator::MIN_LENGTH_CODE, VerificationCodeValidator::MAX_LENGTH_CODE),
            'type' => $type,
            'expires_at' => Carbon::now()->addMinutes($expires_at)
        ]);
    }
    
    /**
     * Get verification code
     * 
     * @param User $user
     * @param string $type
     * @return string
     */
    public function getCode(User $user, string $type): string 
    {
        $verificationCode = $user->VerificationCodes()
                ->where('type', $type)
                ->orderby('id', 'desc')
                ->first();
        
        if ($verificationCode) {
            if ($this->isUsedCode($verificationCode)) {
                return 'Code used up';
            }
            
            if ($this->isExpiredCode($verificationCode)) {
                return 'Code expired';
            }
            
            return (string) $verificationCode->code;
        }
        
        return 'Not found';
    }
    
    /**
     * Check is expired code
     * 
     * @param VerificationCode $verificationCode
     * @return bool
     */
    public function isExpiredCode(VerificationCode $verificationCode): bool 
    {
        $data_now = Carbon::now();
        $expires_at = $verificationCode->expires_at;
        
        return $data_now > $expires_at;
    }
    
    /**
     * Check is Used code
     * 
     * @param VerificationCode $verificationCode
     * @return bool
     */
    public function isUsedCode(VerificationCode $verificationCode): bool
    {
        return $verificationCode->is_used == 1;
    }
    
    /**
     * Set Used up for code
     * 
     * @param User $user
     * @param string $type
     * @return void
     */
    public function setUsedCode(User $user, string $type): void
    {
        $verificationCode = $user->VerificationCodes()
                ->where('type', $type)
                ->orderby('id', 'desc')
                ->first();
        
        if ($verificationCode) {
            $verificationCode->is_used = 1;
            $verificationCode->save();
        }
    }
}
