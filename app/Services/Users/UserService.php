<?php
declare(strict_types=1);

namespace App\Services\Users;

use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Users\UserRepository;
use Illuminate\Support\Facades\Password;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserService
 *
 * @package App\Services\Users
 * @author <freelancer.laravel.vue@gmail.com>
 */
class UserService
{
    /**
     * Index
     * 
     * @return Collection
     */
    public function index(): Collection
    {
        return app(UserRepository::class)->index();
    }
    
    /**
     * Return created User
     *
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public function store(string $name, string $email, string $password): User 
    {
        return app(UserRepository::class)->store(
            $name,
            $email,
            $password
        );
    }

    /**
     * Return bool
     *
     * @param User $user
     * @param string $name
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function update(User $user, string $name, string $email, string $password): bool
    {
        return app(UserRepository::class)->update(
            $user,
            $name,
            $email,
            $password
        );
    }

    /**
     * attachRolesToUser
     *
     * @param User $user
     * @param array|null $role_ids
     * @return void
     */
    public function attachRolesToUser(User $user, ?array $role_ids): void
    {
        $user->roles()->attach($role_ids);
    }

    /**
     * syncRolesToUser
     *
     * @param User $user
     * @param array|null $role_ids
     * @return void
     */
    public function syncRolesToUser(User $user, ?array $role_ids): void
    {
        $user->roles()->sync($role_ids);
    }
    
     /**
     * Set the user's password.
     *
     * @param  User  $user
     * @param  string  $password
     * @return string
     */
    public function setUserPassword(User $user, string $password): string
    {
        $user->password = Hash::make($password);
        $user->save();
        
        return Password::PASSWORD_RESET;
    }
    
    /**
     * Destroy
     * 
     * @param User $user
     * @return void
     */
    public function destroy(User $user): void 
    {
        $user->roles()->detach();
        $user->delete();
    }
}
