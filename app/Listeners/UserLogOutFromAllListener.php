<?php
declare(strict_types=1);

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Events\PasswordReset;

/**
 * Class UserLogOutFromAll
 *
 * @package App\Listeners
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class UserLogOutFromAllListener Implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param PasswordReset $event
     * @return void
     */
    public function handle(PasswordReset $event): void
    {
        $event->user->tokens()->delete();
    }
}
