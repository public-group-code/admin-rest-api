<?php
declare(strict_types=1);

namespace App\DTO\Auth;

/**
 * Class UserRegisterDTO
 *
 * @package App\DTO\Auth
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class UserRegisterDTO
{
    /**
     * @var string Name
     */
    private string $name;

    /**
     * @var string Email
     */
    private string $email;

    /**
     * @var string Password
     */
    private string $password;

    /**
     * Create a new UserRegisterDTO instance.
     *
     * @param string $name
     * @param string $email
     * @param string $password
     */
    public function __construct(string $name, string $email, string $password)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return string
     * @see UserRegisterDTO::$name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     * @see UserRegisterDTO::$email
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     * @see UserRegisterDTO::$password
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
