<?php
declare(strict_types=1);

namespace App\Notifications;

use App\Services\Verifications\VerificationCodeService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;


/**
 * Class EmailVerificationNotification
 *
 * @package App\Notifications
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class EmailVerificationNotification extends Notification implements ShouldQueue
{
    use Queueable;
    
    /**
     * const Code expires at minuts
     * 
     */
    const EXPIRES_AT = 180;
    
    /**
     * const TYPE
     * 
     */
    const TYPE = 'EMAIL_VERIFICATION';

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        $expires_at = self::EXPIRES_AT;
        $verificationCode = app(VerificationCodeService::class)->create(
                $notifiable,
                self::TYPE,
                $expires_at
        );
        $code = $verificationCode->code;

        return (new MailMessage)->view('mails.email-verification', compact('code', 'expires_at'));
    }
}
