<?php

use App\Http\Controllers\Api\V1\Auth\VerificationController;
use App\Http\Controllers\Api\V1\Auth\ForgotPasswordController;
use App\Http\Controllers\Api\V1\Auth\LoginController;
use App\Http\Controllers\Api\V1\Auth\RegisterController;
use App\Http\Controllers\Api\V1\Auth\ResetPasswordController;
use App\Http\Controllers\Api\V1\Roles\RoleController;
use App\Http\Controllers\Api\V1\Users\UserController;
use App\Http\Controllers\Api\V1\Permissions\PermissionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth
Route::prefix('auth')->group(function () {
    Route::post('/login', [LoginController::class, 'login'])->name('login');
    Route::post('/logout', [LoginController::class, 'logout'])->middleware(['auth:api', 'is.expired.token'])->name('logout');
    Route::post('/logout-all', [LoginController::class, 'logoutFromAll'])->middleware(['auth:api', 'is.expired.token']);
    Route::post('/password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
    Route::post('/password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');
    Route::post('/register', [RegisterController::class, 'register']);
    Route::post('/email/verify', [VerificationController::class, 'verify'])->middleware('auth:api')->name('verification.verify');
    Route::post('/email/verification-notification', [VerificationController::class, 'resend'])->middleware(['auth:api'])->name('verification.resend');
});
// Users
Route::prefix('users')->middleware(['auth:api', 'is.expired.token', 'authorize', 'verified'])->group(function () {
    Route::get('/', [UserController::class, 'index']);
    Route::post('/', [UserController::class, 'store']);
    Route::get('/show/{user}', [UserController::class, 'show']);
    Route::put('/update/{user}', [UserController::class, 'update']);
    Route::delete('/delete/{user}', [UserController::class, 'destroy']);
    Route::get('/me', [UserController::class, 'getMe']);
});
// Roles
Route::prefix('roles')->middleware(['auth:api', 'is.expired.token', 'authorize', 'verified'])->group(function () {
    Route::get('/', [RoleController::class, 'index']);
    Route::post('/', [RoleController::class, 'store']);
    Route::get('/show/{role}', [RoleController::class, 'show']);
    Route::put('/update/{role}', [RoleController::class, 'update']);
    Route::delete('/delete/{role}', [RoleController::class, 'destroy']);
});
// Permissions
Route::prefix('permissions')->middleware(['auth:api', 'is.expired.token', 'authorize', 'verified'])->group(function () {
    Route::get('/', [PermissionController::class, 'index']);
    Route::post('/', [PermissionController::class, 'store']);
    Route::get('/show/{permission}', [PermissionController::class, 'show']);
    Route::put('/update/{permission}', [PermissionController::class, 'update']);
    Route::delete('/delete/{permission}', [PermissionController::class, 'destroy']);
});

