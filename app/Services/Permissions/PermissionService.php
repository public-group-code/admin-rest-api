<?php
declare(strict_types=1);

namespace App\Services\Permissions;

use App\Repositories\Permissions\PermissionRepository;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Permissions\Permission;

/**
 * Class PermissionService
 *
 * @package App\Services\Permissions
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PermissionService
{
    /**
     * Index
     * 
     * @return Collection
     */
    public function index(): Collection
    {
        return app(PermissionRepository::class)->index();
    }
    
    /**
     * Return created Permission
     *
     * @param string $name
     * @param string $controller_class
     * @param string $action_method
     * @return Permission
     */
    public function store(string $name, string $controller_class, string $action_method): Permission
    {
        return app(PermissionRepository::class)->store(
            $name,
            $controller_class,
            $action_method
        );
    }

    /**
     * Return bool
     *
     * @param Permission $permission
     * @param string $name
     * @param string $controller_class
     * @param string $action_method
     * @return bool
     */
    public function update(Permission $permission, string $name, string $controller_class, string $action_method): bool
    {
        return app(PermissionRepository::class)->update(
            $permission,
            $name,
            $controller_class,
            $action_method
        );
    }

    /**
     * attachRolesToPermission
     *
     * @param Permission $permission
     * @param array|null $role_ids
     * @return void
     */
    public function attachRolesToPermission(Permission $permission, ?array $role_ids): void
    {
        $permission->roles()->attach($role_ids);
    }

    public function syncRolesToPermission(Permission $permission, ?array $role_ids): void
    {
        $permission->roles()->sync($role_ids);
    }
    
    /**
     * Destroy
     * 
     * @param Permission $permission
     * @return void
     */
    public function destroy(Permission $permission): void 
    {
        $permission->roles()->detach();
        $permission->delete();
    }
}
