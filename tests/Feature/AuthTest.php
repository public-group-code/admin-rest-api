<?php
declare(strict_types=1);

namespace Tests\Feature;

use App\Services\Verifications\VerificationCodeService;
use App\Traits\AuthTest as AuthForTest;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

/**
 * class AuthTest
 *
 * @package Tests\Feature
 * @author <freelancer.laravel.vue@gmail.com>
 */
class AuthTest extends TestCase
{
    use RefreshDatabase, AuthForTest;
    
    /**
     * Register user.
     *
     * @return void
     */
    public function test_register(): void
    {
        $response = $this->post('api/auth/register', [
            'name' => 'test_register',
            'email' => 'test_register@mail.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);
        
        $response->assertNoContent($status = 204);
    }
    
    /**
     * Test_fail_required_name_register.
     *
     * @return void
     */
    public function test_fail_required_name_register(): void
    {
        $response = $this->post('api/auth/register', [
            'name' => '',
            'email' => 'test_fail_required_name_register@mail.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);
        
        $response->assertInvalid(['name' => 'The name field is required.']);
    }
    
    /**
     * Test_fail_required_email_register.
     *
     * @return void
     */
    public function test_fail_required_email_register(): void
    {
        $response = $this->post('api/auth/register', [
            'name' => 'test_fail_required_email_register',
            'email' => '',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);
        
        $response->assertInvalid(['email' => 'The email field is required.']);
    }
    
    /**
     * Test_fail_required_password_register.
     *
     * @return void
     */
    public function test_fail_required_password_register(): void
    {
        $response = $this->post('api/auth/register', [
            'name' => 'test_fail_required_password_register',
            'email' => 'test_fail_required_password_register@mail.com',
            'password' => '',
            'password_confirmation' => ''
        ]);
        
        $response->assertInvalid(['password' => 'The password field is required.']);
    }
    
    /**
     * Test_fail_invalid_password_confirmation_register.
     *
     * @return void
     */
    public function test_fail_invalid_password_confirmation_register(): void
    {
        $response = $this->post('api/auth/register', [
            'name' => 'test_fail_invalid_password_confirmation_register',
            'email' => 'test_fail_invalid_password_confirmation_register@mail.com',
            'password' => 'password123',
            'password_confirmation' => 'password12345'
        ]);
        
        $response->assertInvalid(['password' => 'The password confirmation does not match.']);
    }
    
    /**
     * Login.
     *
     * @return void
     */
    public function test_login(): void
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $response = $this->post('api/auth/login', [
            'email' => $user->email,
            'password' => 'password',
            'device_name' => 'Iphone 14 PRO MAX'
        ]);
        
        $response->assertSuccessful();
    }
    
    /**
     * Logout.
     *
     * @return void
     */
    public function test_logout(): void
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $response = $this->post('api/auth/logout', 
            ['token_id' => $tokenData->token->id], 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );
        
        $response->assertNoContent($status = 204);
    }
    
    /**
     * Logout from all.
     *
     * @return void
     */
    public function test_logout_from_all(): void
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $response = $this->post('api/auth/logout-all', 
            [],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );
        
        $response->assertNoContent($status = 204); 
    }
    
    /**
     * Send Reset Link Email
     * 
     * @return void
     */
    public function test_send_reset_link_email(): void 
    {
        $user = $this->getRegisteredUser();
        $response = $this->post('api/auth/password/email', 
            ['email' => $user->email],
            ['Accept' => 'application/json']
        );
        
        $response->assertSuccessful();
    }
    
    /**
     * Test_fail_required_email_send_reset_link_email
     * 
     * @return void
     */
    public function test_fail_required_email_send_reset_link_email(): void 
    {
        $response = $this->post('api/auth/password/email', 
            ['email' => ''],
            ['Accept' => 'application/json']
        );
        
        $response->assertInvalid(['email' => 'The email field is required.']);
    }
    
    /**
     * Test_password_reset
     * 
     * @return void
     */
    public function test_password_reset(): void
    {
        $user = $this->getRegisteredUser();
        $verificationCode = app(VerificationCodeService::class)->create(
                $user,
                'RESET_PASSWORD',
                10
        );
        $response = $this->post('api/auth/password/reset', 
            [
                'email' => $user->email,
                'code' => $verificationCode->code,
                'password' => 'password123',
                'password_confirmation' => 'password123'
            ],
            ['Accept' => 'application/json']
        );
        
        $response->assertSuccessful();
    }
    
    /**
     * Test_fail_email_required_password_reset
     * 
     * @return void
     */
    public function test_fail_email_required_password_reset(): void 
    {
        $user = $this->getRegisteredUser();
        $verificationCode = app(VerificationCodeService::class)->create(
                $user,
                'RESET_PASSWORD',
                10
        );
        $response = $this->post('api/auth/password/reset', 
            [
                'email' => '',
                'code' => $verificationCode->code,
                'password' => 'password123',
                'password_confirmation' => 'password123'
            ],
            ['Accept' => 'application/json']
        );
        
        $response->assertInvalid(['email' => 'The email field is required.']);
    }
    
    /**
     * Test_fail_code_required_password_reset
     * 
     * @return void
     */
    public function test_fail_code_required_password_reset(): void 
    {
        $user = $this->getRegisteredUser();
        $response = $this->post('api/auth/password/reset', 
            [
                'email' => $user->email,
                'code' => '',
                'password' => 'password123',
                'password_confirmation' => 'password123'
            ],
            ['Accept' => 'application/json']
        );
        
        $response->assertInvalid(['code' => 'The code field is required.']);
    }
    
    /**
     * Test_fail_password_required_password_reset
     * 
     * @return void
     */
    public function test_fail_password_required_password_reset(): void 
    {
        $user = $this->getRegisteredUser();
        $verificationCode = app(VerificationCodeService::class)->create(
                $user,
                'RESET_PASSWORD',
                10
        );
        $response = $this->post('api/auth/password/reset', 
            [
                'email' => $user->email,
                'code' => $verificationCode->code,
                'password' => '',
                'password_confirmation' => 'password123'
            ],
            ['Accept' => 'application/json']
        );
        
        $response->assertInvalid(['password' => 'The password field is required.']);
    }
    
    /**
     * Test_fail_password_required_password_reset
     * 
     * @return void
     */
    public function test_fail_invalid_password_confirmation_password_reset(): void 
    {
        $user = $this->getRegisteredUser();
        $verificationCode = app(VerificationCodeService::class)->create(
                $user,
                'RESET_PASSWORD',
                10
        );
        $response = $this->post('api/auth/password/reset', 
            [
                'email' => $user->email,
                'code' => $verificationCode->code,
                'password' => 'password123',
                'password_confirmation' => 'password12345'
            ],
            ['Accept' => 'application/json']
        );
        
        $response->assertInvalid(['password' => 'The password confirmation does not match.']);
    }
    
    /**
     * Test_email_verification_resend
     * 
     * @return void
     */
    public function test_email_verification_resend(): void
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $response = $this->post('api/auth/email/verification-notification', 
            [],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );
        
        $response->assertSuccessful();
    }
    
    /**
     * Test_email_verify
     * 
     * @return void
     */
    public function test_email_verify(): void
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $verificationCode = app(VerificationCodeService::class)->create(
                $user,
                'EMAIL_VERIFICATION',
                10
        );
        $response = $this->post('api/auth/email/verify', 
            ['code' => $verificationCode->code],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );
        
        $response->assertSuccessful();
    }
}
