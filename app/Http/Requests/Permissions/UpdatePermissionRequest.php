<?php
declare(strict_types=1);

namespace App\Http\Requests\Permissions;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdatePermissionRequest
 *
 * @package App\Http\Requests\Permissions
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class UpdatePermissionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'name' => [
                'bail',
                'required',
                'string',
                'max:255',
            ],
            'controller_class' => [
                'bail',
                'required',
                'string',
                'max:255',
            ],
            'action_method' => [
                'bail',
                'required',
                'string',
                'max:255',
            ],
        ];
    }
}
