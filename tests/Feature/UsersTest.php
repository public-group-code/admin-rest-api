<?php
declare(strict_types=1);

namespace Tests\Feature;

use App\Traits\AuthTest;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

/**
 * class UsersTest
 *
 * @package Tests\Feature
 * @author <freelancer.laravel.vue@gmail.com>
 */
class UsersTest extends TestCase
{
    use RefreshDatabase, AuthTest;
    
    public function test_index(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->get('api/users/', 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(200);
    }
    
    public function test_fail_unauthorized_index(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $response = $this->get('api/users/', 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->post('api/users/', 
            [
                'name' => 'New User',
                'email' => 'newuser@mail.com',
                'password' => 'password',
                'password_confirmation' => 'password'
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(204);
    }
    
    public function test_fail_name_required_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->post('api/users/', 
            [
                'name' => '',
                'email' => 'newuser@mail.com',
                'password' => 'password',
                'password_confirmation' => 'password'
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['name' => 'The name field is required.']);
    }
    
    public function test_fail_email_required_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->post('api/users/', 
            [
                'name' => 'New User',
                'email' => '',
                'password' => 'password',
                'password_confirmation' => 'password'
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['email' => 'The email field is required.']);
    }
    
    public function test_fail_password_required_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->post('api/users/', 
            [
                'name' => 'New User',
                'email' => 'newuser@mail.com',
                'password' => '',
                'password_confirmation' => 'password'
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['password' => 'The password field is required.']);
    }
    
    public function test_fail_invalid_password_confirmation_store(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->post('api/users/', 
            [
                'name' => 'New User',
                'email' => 'newuser@mail.com',
                'password' => 'password',
                'password_confirmation' => 'password_confirmation'
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['password' => 'The password confirmation does not match.']);
    }
    
    public function test_show(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->get('api/users/show/' . $user->id, 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(200);
    }
    
    public function test_fail_unauthorized_show(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $response = $this->get('api/users/show/' . $user->id, 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->put('api/users/update/' . $user->id, 
            [
                'name' => 'Update User',
                'email' => 'update@mail.com',
                'password' => 'password_update',
                'password_confirmation' => 'password_update'
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(204);
    }
    
    public function test_fail_unauthorized_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $response = $this->put('api/users/update/' . $user->id, 
            [
                'name' => 'Update User',
                'email' => 'update@mail.com',
                'password' => 'password_update',
                'password_confirmation' => 'password_update'
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(401);
    }
    
    public function test_fail_name_required_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->put('api/users/update/' . $user->id, 
            [
                'name' => '',
                'email' => 'update@mail.com',
                'password' => 'password_update',
                'password_confirmation' => 'password_update'
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['name' => 'The name field is required.']);
    }
    
    public function test_fail_email_required_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->put('api/users/update/' . $user->id, 
            [
                'name' => 'Update User',
                'email' => '',
                'password' => 'password_update',
                'password_confirmation' => 'password_update'
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['email' => 'The email field is required.']);
    }
    
    public function test_fail_password_required_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->put('api/users/update/' . $user->id, 
            [
                'name' => 'Update User',
                'email' => 'update@mail.com',
                'password' => '',
                'password_confirmation' => ''
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['password' => 'The password field is required.']);
    }
    
    public function test_fail_invalid_password_confirmation_update(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->put('api/users/update/' . $user->id, 
            [
                'name' => 'Update User',
                'email' => 'update@mail.com',
                'password' => 'password_update',
                'password_confirmation' => 'password_confirmation_update'
            ],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertInvalid(['password' => 'The password confirmation does not match.']);
    }
    
    public function test_destroy(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->delete('api/users/delete/' . $user->id, 
            [],
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertNoContent($status = 204);
    }
    
    public function test_getMe(): void 
    {
        Artisan::call('passport:install');
        $user = $this->getRegisteredUser();
        $tokenData = $this->getTokenData($user);
        $user->markEmailAsVerified();
        $this->giveUserRoleWithPermissions($user);
        $response = $this->get('api/users/me/', 
            ['Authorization' => 'Bearer ' . $tokenData->accessToken]
        );

        $response->assertStatus(200);
    }
}
