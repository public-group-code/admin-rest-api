<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Auth;

use Illuminate\Auth\Events\Verified;
use App\Services\Verifications\VerificationCodeService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\Auth\VerificationCodeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class VerificationController
 *
 * @package App\Http\Controllers\Api\V1\Auth
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    /**
     * const TYPE
     * 
     */
    const TYPE = 'EMAIL_VERIFICATION';
    
    /**
     * Mark the authenticated user's email address as verified.
     * 
     * @param VerificationCodeRequest $request
     * @param VerificationCodeService $service
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function verify(VerificationCodeRequest $request, VerificationCodeService $service)
    {
        if (! hash_equals((string) $request->input('code'), (string) $service->getCode($request->user(), self::TYPE))) {
            return new JsonResponse([
                'message' => 'Unauthorized',
            ], 401);
        }

        if ($request->user()->hasVerifiedEmail()) {
            return new JsonResponse([], 204);
        }

        if ($request->user()->markEmailAsVerified()) {
            $service->setUsedCode($request->user(), self::TYPE);
            
            event(new Verified($request->user()));
        }
        
        return new JsonResponse([], 204);
    }
    
    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return new JsonResponse([], 204);
        }

        $request->user()->sendEmailVerificationNotification();

        return new JsonResponse([], 202);
    }
}
