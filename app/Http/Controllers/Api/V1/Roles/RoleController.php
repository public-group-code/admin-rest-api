<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Roles;

use App\Http\Controllers\Controller;
use App\Http\Requests\Roles\StoreRoleRequest;
use App\Http\Requests\Roles\UpdateRoleRequest;
use App\Http\Resources\Roles\RoleResource;
use App\Models\Roles\Role;
use App\Services\Roles\RoleService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class RoleController
 *
 * @package App\Http\Controllers\Api\V1
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $roles = app(RoleService::class)->index();
        $resource = RoleResource::collection($roles);

        return $this->dataResponse($resource->toArray($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRoleRequest  $request
     * @return JsonResponse
     */
    public function store(StoreRoleRequest $request): JsonResponse
    {
        $role = app(RoleService::class)->store(
            $request->name,
            $request->slug
        );
        app(RoleService::class)->attachPermissionsToRole(
            $role,
            $request->permission_ids
        );
        app(RoleService::class)->attachUsersToRole(
            $role,
            $request->user_ids
        );

        return $this->successResponseWithoutContent();
    }

    /**
     * Display the specified resource.
     *
     * @param  Role  $role
     * @return JsonResponse
     */
    public function show(Role $role): JsonResponse
    {
        $resource = new RoleResource($role);

        return $this->dataResponse($resource->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRoleRequest  $request
     * @param  Role  $role
     * @return JsonResponse
     */
    public function update(UpdateRoleRequest $request, Role $role): JsonResponse
    {
        app(RoleService::class)->update(
            $role,
            $request->name,
            $request->slug
        );
        app(RoleService::class)->syncPermissionsToRole(
            $role,
            $request->permission_ids
        );
        app(RoleService::class)->syncUsersToRole(
            $role,
            $request->user_ids
        );

        return $this->successResponseWithoutContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role  $role
     * @return JsonResponse
     */
    public function destroy(Role $role): JsonResponse
    {
        app(RoleService::class)->destroy($role);

        return $this->successResponseWithoutContent();
    }
}
