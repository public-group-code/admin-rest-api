<?php
declare(strict_types=1);

namespace App\Repositories\Users;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

/**
 * Class UserRepository
 *
 * @package App\Repositories\Users
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class UserRepository 
{
    /**
     * Index
     * 
     * @return Collection
     */
    public function index(): Collection  
    {
        return User::with('roles')
            ->where('id', '!=', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->get();
    }
    
    /**
     * Store
     * 
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public function store(string $name, string $email, string $password): User
    {
        return User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);
    }
    
    /**
     * Update
     * 
     * @param User $user
     * @param string $name
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function update(User $user, string $name, string $email, string $password): bool
    {
        return $user->update([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);
    }
}
