<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Auth;

use Illuminate\Support\Facades\Password; 
use App\Services\Users\UserService;
use Illuminate\Auth\Events\PasswordReset;
use App\Models\User;
use App\Services\Verifications\VerificationCodeService;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

/**
 * Class ResetPasswordController
 *
 * @package App\Http\Controllers\Api\V1\Auth
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * const TYPE
     * 
     */
    const TYPE = 'RESET_PASSWORD';
    
    /**
     * Reset the given user's password.
     *
     * @param  ResetPasswordRequest  $request
     * @return JsonResponse
     */
    public function reset(ResetPasswordRequest $request, VerificationCodeService $service): JsonResponse
    {
        $user = User::where('email', $request->input('email'))->first();
        $password = $request->input('password');
        
        if (! $user) {
            return new JsonResponse(['message' => 'Unauthorized'], 401);
        }
           
        if (! hash_equals((string) $request->input('code'), (string) $service->getCode($user, self::TYPE))) {
            return new JsonResponse(['message' => 'Unauthorized'], 401);
        }

        $response = app(UserService::class)->setUserPassword(
                $user,
                $password
        );
        
        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        if ($response == Password::PASSWORD_RESET) {
            $service->setUsedCode($user, self::TYPE);
            
            event(new PasswordReset($user));
            
            return $this->sendResetResponse($request, $response);
        }
        
        return $this->sendResetFailedResponse($request, $response);
    }
}
