<?php
declare(strict_types=1);

namespace App\Repositories\Roles;

use App\Models\Roles\Role;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class RoleRepository
 *
 * @package App\Repositories\Roles
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class RoleRepository 
{
    /**
     * Index
     * 
     * @return Collection
     */
    public function index(): Collection  
    {
        return Role::with('permissions')
            ->with('users')
            ->orderBy('created_at', 'desc')
            ->get();
    }
    
    /**
     * Return created Role
     *
     * @param string $name
     * @param string $slug
     * @return Role
     */
    public function store(string $name, string $slug): Role
    {
        return Role::create([
            'name' => $name,
            'slug' => $slug,
        ]);
    }
    
    /**
     * Return bool
     *
     * @param Role $role
     * @param string $name
     * @param string $slug
     * @return bool
     */
    public function update(Role $role, string $name, string $slug): bool
    {
        return $role->update([
            'name' => $name,
            'slug' => $slug,
        ]);
    }
}
