<?php
declare(strict_types=1);

namespace App\Services\Auth;

use App\DTO\Auth\UserRegisterDTO;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\PersonalAccessTokenResult;

/**
 * Class UserService
 *
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class AuthService
{
    /**
     * @param UserRegisterDTO $DTO
     * @return User
     */
    public function register(UserRegisterDTO $DTO): User
    {
        return User::create([
            'name' => $DTO->getName(),
            'email' => $DTO->getEmail(),
            'password' => Hash::make($DTO->getPassword()),
        ]);
    }

    /**
     * Authenticate
     *
     * @param User $user
     * @param string $device_name
     * @param string|null $remember_me
     * @return PersonalAccessTokenResult
     */
    public function authenticate(User $user, string $device_name, ?string $remember_me): PersonalAccessTokenResult
    {
        $token = $user->createToken($device_name);
        $token->token->expires_at = $remember_me ? Carbon::now()->addMonth() : Carbon::now()->addDay();
        $token->token->save();

        return $token;
    }
}
